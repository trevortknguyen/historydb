var express = require('express');
var app = express();

var db = require('./database');

app.set('port', (process.env.PORT || 3000));
app.set('views', './views');
app.set('view engine', 'pug');

app.use(express.static('public'));

app.get('/', function(req, res) {
  db.query("MATCH (n) RETURN n", function(err, result) {
    res.render('index', {people: result});
  });
});

app.get('/sigma', function(req, res) {
    res.render('sigmatest');
});

app.use(function(req, res) {
  res.render('404');
});

var server = app.listen(app.get('port'), function() {
  console.log("Node app is running on port " + app.get('port'));
});

var io = require('socket.io')(server);
io.on('connection', function(socket) {
  console.log('We are connected.');

  socket.on('disconnect', function() {
    console.log('We are disconnected.');
  });

  socket.on('graph', function(graph) {
    socket.emit('graph', graph);
    console.log('Sending graph.');
    console.log(graph);
  });

  socket.on('request_final_graph', function(callback) {
    var batch = db.batch();

    batch.query("MATCH (n) RETURN n");
    batch.query("MATCH (a)-[r]->(b) RETURN id(r) as id, id(a) as source, id(b) as target");

    batch.commit(function(err, results) {
      if (err) throw err;

      var graph = {
        nodes: [],
        edges: []
      };

      var raw_nodes = results[0];
      raw_nodes.forEach(function(node) {
        node.id = 'n' + node.id;
        node.label = node.firstname + ' ' + node.lastname;
        node.size = 1;
        node.x = Math.random() * 30 - 15;
        node.y = Math.random() * 30 - 15;
        graph.nodes.push(node);
      });

      var raw_edges = results[1];
      raw_edges.forEach(function(edge) {
        edge.id = 'e' + edge.id;
        edge.source = 'n' + edge.source;
        edge.target = 'n' + edge.target;
        graph.edges.push(edge);
      });

      callback(graph);
    });
  });

  socket.on('request_processed_graph', function(callback) {
    var batch = db.batch();

    batch.query("MATCH (n) RETURN n");
    batch.query("MATCH (a)-[r]->(b) RETURN id(r) as id, id(a) as source, id(b) as target");

    batch.commit(function(err, results) {
      if (err) throw err;

      var graph = {
        nodes: [],
        edges: []
      };

      var raw_nodes = results[0];
      raw_nodes.forEach(function(node) {
        graph.nodes.push(node);
      });
      var raw_edges = results[1];
      raw_edges.forEach(function(edge) {
        graph.edges.push(edge);
      });

      callback(graph);
    });
  });

  socket.on('request_graph', function(callback) {
    var batch = db.batch();

    batch.query("MATCH (n) RETURN n");
    batch.query("MATCH (a)-[r]->(b) RETURN id(r) as id, id(a) as source, id(b) as target");

    batch.commit(function(err, results) {
      if (err) throw err;
      callback(results);
    });
  });

  socket.on('name', function(name) {
    var first_name = name.first_name.toLowerCase();
    var last_name = name.last_name.toLowerCase();

    var batch = db.batch();
    batch.queryRaw("MERGE (n:Visitor {firstname: {fname}, lastname: {lname}}) RETURN n", {fname: first_name, lname: last_name});
    batch.queryRaw("MATCH (a), (b) WHERE NOT a = b AND a.lastname = b.lastname CREATE UNIQUE (a)-[r:SHARES_LAST_NAME]-(b)");
    batch.commit(function(err, results) {
      if (err) throw err;
      io.emit('update');
    });
  });
});
